# Data Notes

Notes and questions related to acquiring data from online sources.

## Notes

- Need to have separate datasets by 2010 census polygons vs 2020 census polygons
    + The deprivation indices are only from years with 2010 polygons, so need to use 2020 or earlier ACS variables
    + Potentially create a 2020+ dataset with ACS data with 2020 polygons once updated versions of deprivation indices come out

### Notes by Variable

1. Juvenile court cases
    + Which variables indicate total number of arrests/cases? 
        - Delinquency (petition + non-petition) + status (petition + non-petition)
2. HRSA Area Health Resource Files variables
    + Which columns/variables indicate the desired data (primary care physicians, dentists, mental health providers)?
    + Primary care physicians: "Phys,Primary Care, Patient Care Non-Fed 2019"
    + Dentists: "Dentists w/NPI 2020"
    + Mental health providers:
        - "Psychiatry, Total Patient Care Non-Fed 2019"
        - "Community Mental Health Centers 2020"
        - "Hospitals with social work services"
4. Percent unionized for non-agricultural labor force
    + Online data appears stratified by either state or labor sector, but not both, so will need to use state-level values that include all labor sectors -- **approved by Moronke.**
5. Variables from ACS: Will need to use 2020 or earlier data for now 

## To Do

1. Juvenile court cases
    - Download most recent year of data for each state separately and combine into a long format table. Need to add the following columns for each file:
        + State
        + Year
2. Healthcare providers from HRSA
    - Download and extract variables listed
3. Minimum wage by state
    - Extract online table into long-format table with columns:
        + State
        + Wage amount
4. Percent unionized for non-agricultural labor force
    - Download each state's value separately and combine into single table and add columns:
        + State
        + Value
5. Food swamp
    - From PhenX:  The presence of a food swamp is calculated using the traditional Retail Food Environment Index per county, which includes the number of fast food restaurants and convenience stores divided by the number of grocery stores and supermarkets. The number of grocery stores, fast food restaurants, and convenience stores is determined at a county level using data from the **Food Environment Atlas**.
    - Traditional Retail Food Environment Index (RFEI) = (Fast Food / Limited Service Establishments + Convenience Stores) / (Grocery Stores / Super Markets)
        + RFEI = (FFR16 + CONVS16) / (GROC16 + SUPERC16)
    - **To do:**
        + Make "StateAndCountyData" into wide format with counties in rows and variables in columns
        + Derive RFEI as above using variables from Food Environment Atlas
