# Managing SDOH Data

**General Notes:**

- For reproducibility, the analyses herein use RStudio via a geospatial Singularity container with current verisons of common open-source R tools
    + The container repo is [here](https://gitlab.oit.duke.edu/ts415/geospatial-processing-singularity)
    + The container recipe file is [here](https://gitlab.oit.duke.edu/ts415/geospatial-processing-singularity/-/blob/main/Singularity.def)
    + The container can be pulled from Duke's ORAS server with the following Singularity command:

`singularity pull oras://gitlab-registry.oit.duke.edu/ts415/geospatial-processing-singularity:latest`

## Approach

- Patient locations will hopefully consist of full addresses and not simply of postal ZIPs. This will allow precise determination of appropriate areal polygons for each patient and will virtually eliminate error relative to using ZIPs. See [Wilson and Din 2018](https://mdsoar.org/handle/11603/11293)
- In either case, given the privacy issues with sharing patient location data, geoprocessing will likely occur within each institution prior to sharing:
    1. Download shared SDOH datasets
    2. Pull patient data
    3. Geocode patient addresses
    4. Determine intersecting areal polygons for each patient
        - Census block groups
        - Census tracts
        - ZCTA
        - County
    5. Left join of patient records with SDOH datasets
        - Allow patient to not have matching SDOH data, but not reverse
    6. Deidentify data
        - Remove addresses, geolocations, FIPS of areal polygons
        - Jitter variables
            + Numeric: Add Normal noise
            + Categorical: ? 
            + Binary: No need? 
    7. Add institution-level variables
        - De-identified institution code
    8. Share de-identified data with consortium partners
    9. Merge all patient data

## Specific Methods

### Preparing SDOH Datasets

This will be a general-use dataset that will be provided to each institution for merging with patient records

1. Create meta dataset of desired variables
    - Variable name
    - Link to data file online
    - Year
    - Data file name
    - Data file type (extension)
    - Spatial unit of data
        + Census tract
        + Census block group
        + ZCTA
        + County
2. Download data files
    - Organize files into directories by spatial unit
    - Upload to this repo in /data/data_files
3. Join data files by spatial unit
    1. Create a vector of paths for local files
    2. Load files as list by looping over paths
    3. Iteratively **outer join** by FIPS
        - Allows variables to be missing spatial units
4. Data checks
    - Missing values
    - Other?
5. Upload to this repo in data/data_files for sharing

### Geocoding Patient Addresses

- This will need to be done "in house" by each institution's data teams
- **Do we need to provide code?**

### Merging SDOH Data with Patient Records

1. Load geocoded patient records
2. Load areal polygons using 'tigris'
    1. BGs
    2. CTs
    3. ZCTAs
3. Intersect patient point locations with areal polygons by type (point-polygon intersection) using 'sf'
    - This provides a FIPS for each of the above areal polygons for each patient
4. Derive county FIPS using BG FIPS (nested)
5. Load SDOH datasets (one for each areal polygon type)
6. Iteratively outer join patient records with SDOH datasets using appropriate patient FIPS
